package com.pathashala57.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MovieTest {

    @Test
    void getStringRepresentation() {

        Movie aMovie = new Movie("I", "Shankar", 2015);
        String headerformat = "%-35s %-35s %-35s";
        String expectedStringRepresentation = String.format(headerformat, "I", "Shankar", 2015);

        assertEquals(expectedStringRepresentation, aMovie.getStringRepresentation());
    }

    @Test
    void hasNameReturnsTrueWhenNameOfMovieMatches() {
        Movie aMovie = new Movie("I", "Shankar", 2015);

        assertTrue(aMovie.hasName("I"));
    }

    @Test
    void hasNameReturnsFalseWhenNameOfBookIsIncorrect() {
        Movie aMovie = new Movie("I", "Shankar", 2015);

        assertFalse(aMovie.hasName("The Alchemis"));
    }

}