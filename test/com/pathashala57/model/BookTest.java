package com.pathashala57.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookTest {

    @Test
    void getStringRepresentation() {
        Book aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);
        String headerformat = "%-35s %-35s %-35s";
        String expectedStringRepresentation = String.format(headerformat, "The Alchemist", "Paulo Cohelo", 1988);

        assertEquals(expectedStringRepresentation, aBook.getStringRepresentation());
    }

    @Test
    void hasNameReturnsTrueWhenNameOfBookIsMatches() {
        Book aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);

        assertTrue(aBook.hasName("The Alchemist"));
    }

    @Test
    void hasNameReturnsFalseWhenNameOfBookIsIncorrect() {
        Book aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);

        assertFalse(aBook.hasName("The Alchemis"));
    }

    @Test
    void bookEqualsAnotherBookWithSameName() {
        Book aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);
        Book anotherBook = new Book("The Alchemist", "Paulo Cohelo", 1988);

        assertEquals(aBook, anotherBook);
    }

    @Test
    void bookNotEqualAnotherBookWithDifferentName() {
        Book aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);
        Book anotherBook = new Book("The Alchemist 2", "Paulo Cohelo", 1988);

        assertNotEquals(aBook, anotherBook);
    }

    @Test
    void bookNotEqualObjectWithDifferentClass() {
        Book aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);

        assertNotEquals(aBook, "The Alchemist");
    }

    @Test
    void bookNotEqualToNull() {
        Book aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);

        assertNotEquals(aBook, null);
    }

}