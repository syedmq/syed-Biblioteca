package com.pathashala57.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void verifyUserCredentialsSuccessful() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");

        assertTrue(aUser.verifyCredentials("21093", "password"));
    }

    @Test
    void verifyUserCredentialsUnsuccessful() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");

        assertFalse(aUser.verifyCredentials("21093", "password2"));
    }

    @Test
    void userEqualsAnotherUserWithSameId() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        User anotherUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");

        assertEquals(anotherUser, aUser);
    }

    @Test
    void userNotEqualsAnotherUserWithDifferentId() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        User anotherUser = new User("Qasim", "21094", "password", "8853530652", "smdqsm@gmail.com");

        assertNotEquals(anotherUser, aUser);
    }

    @Test
    void userNotEqualsAnotherObjectWithDiffrentClass() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");

        assertNotEquals(new Object(), aUser);
    }

    @Test
    void userNotEqualsNull() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");

        assertNotEquals(aUser, null);
    }

}