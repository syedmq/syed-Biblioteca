package com.pathashala57.model;

import com.pathashala57.view.Console;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class BibliotecaTest {

    public static final String NO_BOOKS = "No Books In The Library.Please visit after sometime!";
    private List<LibraryItem> libraryItems;
    private Biblioteca bangalorePublicLibrary;
    private Book aBook, anotherBook;
    private User aUser;

    @BeforeEach
    void intialize() {
        Console console = mock(Console.class);
        libraryItems = new ArrayList<>();
        aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);
        libraryItems.add(aBook);
        anotherBook = new Book("Thousand Splendid Sons", "Khaled Husseni", 2007);
        libraryItems.add(anotherBook);
        bangalorePublicLibrary = new Biblioteca(libraryItems, new ArrayList<>());
        aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
    }

    @Test
    void libraryHasNoBook() {
        Console console = mock(Console.class);
        List<LibraryItem> booksInLibrary = new ArrayList<>();
        Biblioteca bangalorePublicLibrary = new Biblioteca(booksInLibrary, new ArrayList<>());

        assertTrue(bangalorePublicLibrary.hasNoItemsOf(Book.class));
    }

    @Test
    void listBooksWithAuthorAndDate() {
        assertEquals(aBook.getStringRepresentation() + System.lineSeparator()
                        + anotherBook.getStringRepresentation()
                        + System.lineSeparator(),
                bangalorePublicLibrary.listOfItems(Book.class));
    }

    @Test
    void checkoutABookReturnsTheBook() {
        Optional<LibraryItem> bookGotFromLibrary = bangalorePublicLibrary
                .checkout("The Alchemist", Book.class, null);

        assertEquals(aBook, bookGotFromLibrary.get());
    }


    @Test
    void unSuccesfullyCheckoutABookreturnsNull() {
        Optional<LibraryItem> bookGotFromLibrary = bangalorePublicLibrary
                .checkout("The A", Book.class, null);

        assertFalse(bookGotFromLibrary.isPresent());
    }

    @Test
    void successfullyCheckingOutABookRemovesTheBook() {
        Optional<LibraryItem> bookGotFromLibrary = bangalorePublicLibrary
                .checkout("The Alchemist", Book.class, null);
        Optional<LibraryItem> anotherBookGotFromLibrary = bangalorePublicLibrary
                .checkout("The Alchemist", Book.class, null);

        assertEquals(aBook, bookGotFromLibrary.get());
        assertFalse(anotherBookGotFromLibrary.isPresent());
    }

    @Test
    void returnAbookSuccessfully() {
        Optional<LibraryItem> bookGotFromLibrary = bangalorePublicLibrary
                .checkout("The Alchemist", Book.class, aUser);

        assertTrue(bangalorePublicLibrary.returnItem("The Alchemist", Book.class, aUser));
        Optional<LibraryItem> anotherBookGotFromLibrary = bangalorePublicLibrary
                .checkout("The Alchemist", Book.class, null);
        assertEquals(aBook, anotherBookGotFromLibrary.get());
    }

    @Test
    void returnAbookUnSuccessfully() {
        assertFalse(bangalorePublicLibrary.returnItem("The Alchemist", Book.class, aUser));
    }

    @Test
    void listMovies() {
        Movie aMovie = new Movie("I", "Shankar", 2015);
        libraryItems.add(aMovie);
        Biblioteca library = new Biblioteca(libraryItems, new ArrayList<>());

        assertEquals(aMovie.getStringRepresentation() + System.lineSeparator(),
                library.listOfItems(Movie.class));
    }

    @Test
    void checkoutAMovieReturnsTheMovie() {
        Movie aMovie = new Movie("I", "Shankar", 2015);
        libraryItems.add(aMovie);
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        Biblioteca library = new Biblioteca(libraryItems, new ArrayList<>());

        Optional<LibraryItem> bookGotFromLibrary = library.checkout("I", Movie.class, aUser);

        assertEquals(aMovie, bookGotFromLibrary.get());
    }

    @Test
    void successfullyReturnsTheMovie() {
        Movie aMovie = new Movie("I", "Shankar", 2015);
        libraryItems.add(aMovie);
        Biblioteca library = new Biblioteca(libraryItems, new ArrayList<>());
        library.checkout("I", Movie.class, aUser);

        assertTrue(library.returnItem("I", Movie.class, aUser));
    }

    @Test
    void userLoginSuccessful() {
        List<User> users = new ArrayList<>();
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        users.add(aUser);
        Biblioteca biblioteca = new Biblioteca(libraryItems, users);

        assertEquals(biblioteca.verifyUser("21093", "password").get(), aUser);
    }

    @Test
    void userLoginUnSuccessful() {
        List<User> users = new ArrayList<>();
        users.add(new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com"));
        Biblioteca biblioteca = new Biblioteca(libraryItems, users);

        assertFalse(biblioteca.verifyUser("21093", "password1").isPresent());
    }

}
