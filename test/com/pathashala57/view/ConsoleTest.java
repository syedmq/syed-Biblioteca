package com.pathashala57.view;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import static org.mockito.Mockito.*;


class ConsoleTest {

    @Test
    void displaysMessage() {
        PrintStream outputStream = mock(PrintStream.class);
        Console console = new Console(outputStream);

        console.display("Welcome to Bangalore Public Library!");

        verify(outputStream).println("Welcome to Bangalore Public Library!");
    }

    @Test
    void getsInput() throws IOException {
        BufferedReader inputReader = mock(BufferedReader.class);
        Console console = new Console(inputReader);
        when(console.getInput()).thenReturn("1");

        console.getInput();

        verify(inputReader).readLine();
    }

}