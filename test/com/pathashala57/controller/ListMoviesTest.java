package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.model.Movie;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;


class ListMoviesTest {
    @Test
    void displayNoMoviesIfListEmpty() {
        Biblioteca library = mock(Biblioteca.class);
        Console console = mock(Console.class);
        ListMovies optionToListBooks = new ListMovies();
        when(library.hasNoItemsOf(Movie.class)).thenReturn(true);

        optionToListBooks.execute(library, console, null);

        verify(library).hasNoItemsOf(Movie.class);
        verify(console).display("No Movies In The Library.Please visit after sometime!");
    }

    @Test
    void displayMovies() {
        Biblioteca library = mock(Biblioteca.class);
        Console console = mock(Console.class);
        ListMovies optionToListBooks = new ListMovies();
        when(library.hasNoItemsOf(Movie.class)).thenReturn(true);

        optionToListBooks.execute(library, console, null);

        verify(library).hasNoItemsOf(Movie.class);
        verify(console).display("No Movies In The Library.Please visit after sometime!");
    }

}