package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.model.Book;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class ListBooksTest {

    @Test
    void displaysTheListOfBooks() {
        Biblioteca library = mock(Biblioteca.class);
        Console console = mock(Console.class);
        ListBooks optionToListBooks = new ListBooks();
        String headerformat = "%-35s %-35s %-35s";
        String header = String.format(headerformat, "Name", "Author", "Year Published");
        when(library.hasNoItemsOf(Book.class)).thenReturn(false);


        optionToListBooks.execute(library, console, null);

        verify(library).hasNoItemsOf(Book.class);
        verify(library).listOfItems(Book.class);
        verify(console).display(header);
        verify(console).display("The Books In The Library Are:");
    }

    @Test
    void displayNoBooksAvailable() {
        Biblioteca library = mock(Biblioteca.class);
        Console console = mock(Console.class);
        ListBooks optionToListBooks = new ListBooks();
        when(library.hasNoItemsOf(Book.class)).thenReturn(true);

        optionToListBooks.execute(library, console, null);

        verify(library).hasNoItemsOf(Book.class);
        verify(console).display("No Books In The Library.Please visit after sometime!");
    }

}