package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.model.Movie;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class ReturnMovieTest {
    @Test
    void unsuccessfullyReturnAMovie() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");

        ReturnMovie optionToReturnBook = new ReturnMovie();
        Console console = mock(Console.class);
        Biblioteca library = mock(Biblioteca.class);
        when(console.getInput()).thenReturn("aMovie");
        when((library.returnItem("aMovie", Movie.class, aUser))).thenReturn(false);

        optionToReturnBook.execute(library, console, aUser);

        verify(library).returnItem("aMovie", Movie.class, aUser);
        verify(console).display("That is not a valid Movie to return.");
    }

    @Test
    void returnAMovieSuccessfully() {
        ReturnMovie optionToReturnMovie = new ReturnMovie();
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        Console console = mock(Console.class);
        Biblioteca library = mock(Biblioteca.class);
        when(console.getInput()).thenReturn("aMovie");
        when((library.returnItem("aMovie", Movie.class, aUser))).thenReturn(true);

        optionToReturnMovie.execute(library, console, aUser);

        verify(library).returnItem("aMovie", Movie.class, aUser);
        verify(console).display("Thank you for returning the Movie.");
    }

}