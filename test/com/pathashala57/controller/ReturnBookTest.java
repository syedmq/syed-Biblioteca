package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.model.Book;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;


class ReturnBookTest {

    @Test
    void returnsAbookSuccessful() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        ReturnBook optionToReturnBook = new ReturnBook();
        Console console = mock(Console.class);
        Biblioteca library = mock(Biblioteca.class);
        when(console.getInput()).thenReturn("abook");
        when((library.returnItem("abook", Book.class, aUser))).thenReturn(true);

        optionToReturnBook.execute(library, console, aUser);

        verify(library).returnItem("abook", Book.class, aUser);
        verify(console).display("Thank you for returning the book.");
    }
    @Test
    void returnsAbookUnSuccessful() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        ReturnBook optionToReturnBook = new ReturnBook();
        Console console = mock(Console.class);
        Biblioteca library = mock(Biblioteca.class);
        when(console.getInput()).thenReturn("abook");
        when((library.returnItem("abook", Book.class, aUser))).thenReturn(false);

        optionToReturnBook.execute(library, console, aUser);

        verify(library).returnItem("abook", Book.class, aUser);
        verify(console).display("That is not a valid book to return.");
    }

}