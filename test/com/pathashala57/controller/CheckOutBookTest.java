package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.model.Book;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.mockito.Mockito.*;

class CheckOutBookTest {
    Console console;
    private Book aBook;

    @BeforeEach
    void intialize() {
        console = mock(Console.class);
        aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);
    }

    @Test
    void successfullyCheckoutABook() {

        Biblioteca library = mock(Biblioteca.class);
        Console console = mock(Console.class);
        CheckOutBook checkOutBook = new CheckOutBook();
        when(console.getInput()).thenReturn("The Alchemist");
        when(library.checkout("The Alchemist", Book.class, null)).thenReturn(Optional.of(aBook));

        checkOutBook.execute(library, console, null);

        verify(library).checkout("The Alchemist", Book.class, null);
        verify(console).display("Thank you! Enjoy the book!");
    }

    @Test
    void unsuccessfullyCheckoutABook() {
        Biblioteca library = mock(Biblioteca.class);
        Console console = mock(Console.class);
        CheckOutBook checkOutBook = new CheckOutBook();
        when(console.getInput()).thenReturn("The Al");
        when(library.checkout("The Al", Book.class, null)).thenReturn(Optional.empty());

        checkOutBook.execute(library, console, null);

        verify(library).checkout("The Al", Book.class, null);
        verify(console).display("That book is not available.");
    }

}