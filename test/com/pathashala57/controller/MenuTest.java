package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.model.LibraryItem;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

public class MenuTest {
    @Test
    void printsWelcomeMessage() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        Console console = mock(Console.class);
        Biblioteca library = mock(Biblioteca.class);
        Menu menu = new Menu(console, library, Runner.getLoggegInMenuMap(), aUser);

        menu.displayWelcomeMessage();

        verify(console).display("Welcome to Bangalore Public Library!");
    }

    @Test
    void printsChoices() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");

        Console console = mock(Console.class);
        List<LibraryItem> books = new ArrayList<>();
        Biblioteca library = new Biblioteca(books, new ArrayList<>());
        Menu menu = new Menu(console, library, Runner.getMainMenuMap(), aUser);
        when(console.getInput()).thenReturn("quit");

        menu.showChoices();

        verify(console).display("Menu:");
        verify(console).display("Please Enter Your Choice:");
    }

    @Test
    void menuExecutesCommand() {
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");

        Map<String, MenuOption> menuMap = new HashMap<>();
        ListBooks menuItem = mock(ListBooks.class);
        menuMap.put("1", menuItem);
        menuMap.put("quit", new QuitMenu());
        Console console = mock(Console.class);
        Biblioteca biblioteca = mock(Biblioteca.class);
        Menu menu = new Menu(console, biblioteca, menuMap, aUser);
        when(console.getInput()).thenReturn("1").thenReturn("quit");

        menu.showChoices();

        verify(menuItem).execute(biblioteca, console, aUser);
    }

}
