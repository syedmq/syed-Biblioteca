package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class QuitMenuTest {

    @Test
    void displaysThankYouMessage() {
        QuitMenu optionToQuit = new QuitMenu();
        Console console = mock(Console.class);
        Biblioteca library = mock(Biblioteca.class);

        optionToQuit.execute(library, console, null);

        verify(console).display("Thank You For Visiting");
    }

}
