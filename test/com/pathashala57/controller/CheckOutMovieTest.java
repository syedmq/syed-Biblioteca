package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.model.LibraryItem;
import com.pathashala57.model.Movie;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;


class CheckOutMovieTest {

    Console console;
    private Movie aMovie;

    @BeforeEach
    void intialize() {
        console = mock(Console.class);
        List<LibraryItem> booksInLibrary = new ArrayList<>();
        aMovie = new Movie("I", "Paulo Cohelo", 1988);
        booksInLibrary.add(aMovie);
        Movie anotherMovie = new Movie("A seperation", "Khaled Husseni", 2007);
        booksInLibrary.add(anotherMovie);
    }

    @Test
    void successfullyCheckOutAMovie() {
        Biblioteca biblioteca = mock(Biblioteca.class);
        Console console = mock(Console.class);
        when(console.getInput()).thenReturn("I");
        when(biblioteca.checkout("I", Movie.class, null)).thenReturn(Optional.of(aMovie));
        CheckOutMovie checkOutMovie = new CheckOutMovie();

        checkOutMovie.execute(biblioteca, console, null);

        verify(biblioteca).checkout("I", Movie.class, null);
        verify(console).display("Thank you! Enjoy the Movie!");
    }

    @Test
    void UnsuccessfullyCheckOutAMovie() {
        Biblioteca biblioteca = mock(Biblioteca.class);
        Console console = mock(Console.class);
        when(console.getInput()).thenReturn("I");
        when(biblioteca.checkout("I", Movie.class, null)).thenReturn(Optional.empty());
        CheckOutMovie checkOutMovie = new CheckOutMovie();

        checkOutMovie.execute(biblioteca, console, null);

        verify(biblioteca).checkout("I", Movie.class, null);
        verify(console).display("That Movie is not available.");
    }

}