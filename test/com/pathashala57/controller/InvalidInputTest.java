package com.pathashala57.controller;

import com.pathashala57.model.Biblioteca;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class InvalidInputTest {

    @Test
    void diplaysInvalidInputMessage() {
        Console console = mock(Console.class);
        Biblioteca biblioteca = mock(Biblioteca.class);
        InvalidInput invalidInput = new InvalidInput();

        invalidInput.execute(biblioteca, console, null);

        verify(console).display("Please Enter Correct Choice");
    }

}
