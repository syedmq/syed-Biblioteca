package com.pathashala57.controller;

import com.pathashala57.model.Library;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.mockito.Mockito.*;


class LoginTest {
    @Test
    void unsuccessfulLogin() {
        Console console = mock(Console.class);
        Library library = mock(Library.class);
        Login optionToLogin = new Login();
        when(console.getInput()).thenReturn("21093").thenReturn("password1");
        when(library.verifyUser("21093", "password1")).thenReturn(Optional.empty());
        optionToLogin.execute(library, console, null);

        verify(library).verifyUser("21093", "password1");
        verify(console).display("Wrong Credentials!");
    }

    @Test
    void successfulLogin() {
        Console console = mock(Console.class);
        Library library = mock(Library.class);
        Login optionToLogin = new Login();
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        when(console.getInput()).thenReturn("21093").thenReturn("password1").thenReturn("logout");
        when(library.verifyUser("21093", "password1")).thenReturn(Optional.of(aUser));
        optionToLogin.execute(library, console, null);

        verify(library).verifyUser("21093", "password1");
    }

}