package com.pathashala57.controller;

import com.pathashala57.model.Book;
import com.pathashala57.model.Library;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

//Represents an option that displays all the books
public class ListBooks implements MenuOption {

    private static final String NO_BOOKS_IN_THE_LIBRARY_PLEASE_VISIT_AFTER_SOMETIME =
            "No Books In The Library.Please visit after sometime!";
    private static final String DISPLAY_FORMAT = "%-35s %-35s %-35s";
    private static final String THE_BOOKS_IN_THE_LIBRARY_ARE = "The Books In The Library Are:";
    private static final String name = "List Books";

    public String getName() {
        return name;
    }

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        if (library.hasNoItemsOf(Book.class)) {
            console.display(NO_BOOKS_IN_THE_LIBRARY_PLEASE_VISIT_AFTER_SOMETIME);
            return;
        }
        String header = String.format(DISPLAY_FORMAT, "Name", "Author", "Year Published");
        String boookDetails = library.listOfItems(Book.class);
        console.display(THE_BOOKS_IN_THE_LIBRARY_ARE);
        console.display(header);
        console.display(boookDetails);
    }

}
