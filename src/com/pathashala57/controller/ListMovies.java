package com.pathashala57.controller;

import com.pathashala57.model.Library;
import com.pathashala57.model.Movie;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

//Represents an option to list movies
public class ListMovies implements MenuOption {

    private static final String NO_MOVIES_IN_THE_LIBRARY_PLEASE_VISIT_AFTER_SOMETIME =
            "No Movies In The Library.Please visit after sometime!";
    private static final String THE_MOVIES_IN_THE_LIBRARY_ARE = "The Movies In The Library Are:";
    private static final String DISPLAY_FORMAT = "%-35s %-35s %-35s";
    private static String name = "List Movies";

    public String getName() {
        return name;
    }

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        if (library.hasNoItemsOf(Movie.class)) {
            console.display(NO_MOVIES_IN_THE_LIBRARY_PLEASE_VISIT_AFTER_SOMETIME);
            return;
        }
        String header = String.format(DISPLAY_FORMAT, "Name", "Director", "Year Released");
        console.display(THE_MOVIES_IN_THE_LIBRARY_ARE);
        String movieDetails = library.listOfItems(Movie.class);
        console.display(header);
        console.display(movieDetails);
    }

}
