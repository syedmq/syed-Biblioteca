package com.pathashala57.controller;


import com.pathashala57.model.Book;
import com.pathashala57.model.Library;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

//Represents option to return book
public class ReturnBook implements MenuOption {

    private static final String PLEASE_ENTER_THE_NAME_OF_THE_BOOK_YOU_WISH_TO_RETURN =
            "Please Enter The Name Of The Book You Wish To Return";
    private static final String THANK_YOU_FOR_RETURNING_THE_BOOK =
            "Thank you for returning the book.";
    private static final String THAT_IS_NOT_A_VALID_BOOK_TO_RETURN =
            "That is not a valid book to return.";
    private static final String name = "Return Book";

    public String getName() {
        return name;
    }

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        console.display(PLEASE_ENTER_THE_NAME_OF_THE_BOOK_YOU_WISH_TO_RETURN);
        String bookNameToReturn = console.getInput();
        if (library.returnItem(bookNameToReturn, Book.class, loggedUser)) {
            console.display(THANK_YOU_FOR_RETURNING_THE_BOOK);
        } else {
            console.display(THAT_IS_NOT_A_VALID_BOOK_TO_RETURN);
        }
    }

}
