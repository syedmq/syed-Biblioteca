package com.pathashala57.controller;

import com.pathashala57.model.Library;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

//Represents the option to quit the menu
public class QuitMenu implements MenuOption {

    private static final String THANK_YOU_FOR_VISITING = "Thank You For Visiting";
    private static final String name = "Exit.";

    public String getName() {
        return name;
    }

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        console.display(THANK_YOU_FOR_VISITING);
    }

}
