package com.pathashala57.controller;

import com.pathashala57.model.Library;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

//Represents the Options that menu has
public interface MenuOption {

    void execute(Library library, Console console, User loggedUser);

    String getName();
}
