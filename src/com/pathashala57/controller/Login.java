package com.pathashala57.controller;

import com.pathashala57.model.Library;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

import java.util.Optional;

//Represents option for user to sign in
public class Login implements MenuOption {

    private static final String PLEASE_ENTER_YOUR_ID = "Please Enter Your ID:";
    private static final String PLEASE_ENTER_YOUR_PASSWORD = "Please Enter Your Password";
    private static final String DISPLAY_FORMAT = "%-35s %-10s %-20s %-20s";
    private static final String WRONG_CREDENTIALS = "Wrong Credentials!";
    private static final String name = "Login";

    public String getName() {
        return name;
    }

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        console.display(PLEASE_ENTER_YOUR_ID);
        String id = console.getInput();
        console.display(PLEASE_ENTER_YOUR_PASSWORD);
        String password = console.getInput();
        Optional<User> user = library.verifyUser(id, password);
        if (user.isPresent()) {
            String header = String.format(DISPLAY_FORMAT, "Name", "ID", "Phone", "Email");
            console.display(header);
            console.display(user.get().stringRepresentation());
            Menu menu = new Menu(console, library, Runner.getLoggegInMenuMap(), user.get());
            menu.run();
        } else {
            console.display(WRONG_CREDENTIALS);
        }
    }

}
