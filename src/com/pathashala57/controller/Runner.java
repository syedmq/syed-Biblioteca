package com.pathashala57.controller;

import com.pathashala57.model.*;
import com.pathashala57.view.Console;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

//Represents a Launcher For Biblioteca
public class Runner {

    public static void main(String[] args) {
        Console consoleOutput = new Console(System.out, new BufferedReader(new InputStreamReader(System.in)));
        Biblioteca bangalorePublicLibrary = new Biblioteca(getLibraryItems(), getUsers());
        Menu menu = new Menu(consoleOutput, bangalorePublicLibrary, getMainMenuMap(), null);
        menu.run();
    }

    private static List<LibraryItem> getLibraryItems() {
        List<LibraryItem> libraryItems = new ArrayList<>();
        Book aBook = new Book("The Alchemist", "Paulo Cohelo", 1988);
        Book anotherBook = new Book("A Thousand Splendid Suns", "Khaled Hosseini", 2007);
        Movie aMovie = new Movie("I", "Shankar", 2015);
        Movie anotherMovie = new Movie("Roy", "Shankar2", 2015);

        libraryItems.add(aMovie);
        libraryItems.add(anotherMovie);
        libraryItems.add(aBook);
        libraryItems.add(anotherBook);
        return libraryItems;
    }

    private static List<User> getUsers() {
        List<User> users = new ArrayList<>();
        User aUser = new User("Qasim", "21093", "password", "8853530652", "smdqsm@gmail.com");
        User anotherUser = new User("Jay", "21094", "password1", "8853530652", "smdqsm@gmail.com");
        users.add(aUser);
        users.add(anotherUser);
        return users;
    }

    static Map<String, MenuOption> getLoggegInMenuMap() {
        TreeMap<String, MenuOption> menuMap = new TreeMap<>();
        menuMap.put("1", new ListBooks());
        menuMap.put("2", new CheckOutBook());
        menuMap.put("3", new ReturnBook());
        menuMap.put("4", new ListMovies());
        menuMap.put("5", new CheckOutMovie());
        menuMap.put("6", new ReturnMovie());
        menuMap.put("logout", new QuitMenu());
        return menuMap;
    }

    static Map<String, MenuOption> getMainMenuMap() {
        Map<String, MenuOption> menuMap = new HashMap<>();
        menuMap.put("1", new ListBooks());
        menuMap.put("2", new ListMovies());
        menuMap.put("3", new Login());
        menuMap.put("quit", new QuitMenu());
        return menuMap;
    }

}
