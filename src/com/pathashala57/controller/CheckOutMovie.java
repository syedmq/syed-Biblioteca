package com.pathashala57.controller;

import com.pathashala57.model.Library;
import com.pathashala57.model.LibraryItem;
import com.pathashala57.model.Movie;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

import java.util.Optional;

//Represents option to check out movies
public class CheckOutMovie implements MenuOption {

    private static final String ENTER_THE_MOVIE_NAME_YOU_WANT_TO_CHECKOUT =
            "Enter the Movie Name You Want To Checkout";
    private static final String THANK_YOU_ENJOY_THE_MOVIE = "Thank you! Enjoy the Movie!";
    private static final String THAT_MOVIE_IS_NOT_AVAILABLE = "That Movie is not available.";
    private static final String name = "Check Out A  Movie";

    public String getName() {
        return name;
    }

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        console.display(ENTER_THE_MOVIE_NAME_YOU_WANT_TO_CHECKOUT);
        String movieNameToCheckout = console.getInput();
        Optional<LibraryItem> movieTocheckOut = library.checkout(movieNameToCheckout, Movie.class, loggedUser);
        if (movieTocheckOut.isPresent()) {
            console.display(THANK_YOU_ENJOY_THE_MOVIE);
        } else {
            console.display(THAT_MOVIE_IS_NOT_AVAILABLE);
        }
    }

}
