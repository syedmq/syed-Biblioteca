package com.pathashala57.controller;

import com.pathashala57.model.Library;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

//Represents option when input is invalid
public class InvalidInput implements MenuOption {

    private static final String PLEASE_ENTER_CORRECT_CHOICE = "Please Enter Correct Choice";
    private static final String INVALID_INPUT = "Invalid Input";

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        console.display(PLEASE_ENTER_CORRECT_CHOICE);
    }

    @Override
    public String getName() {
        return INVALID_INPUT;
    }

}
