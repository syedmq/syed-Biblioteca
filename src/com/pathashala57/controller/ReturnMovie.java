package com.pathashala57.controller;


import com.pathashala57.model.Library;
import com.pathashala57.model.Movie;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

//Represents option to return movie
public class ReturnMovie implements MenuOption {

    private static final String PLEASE_ENTER_THE_NAME_OF_THE_MOVIE_YOU_WISH_TO_RETURN =
            "Please Enter The Name Of The Movie You Wish To Return";
    private static final String THANK_YOU_FOR_RETURNING_THE_MOVIE =
            "Thank you for returning the Movie.";
    private static final String THAT_IS_NOT_A_VALID_MOVIE_TO_RETURN =
            "That is not a valid Movie to return.";
    private static final String name = "Return Movie";

    public String getName() {
        return name;
    }

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        console.display(PLEASE_ENTER_THE_NAME_OF_THE_MOVIE_YOU_WISH_TO_RETURN);
        String bookNameToReturn = console.getInput();
        if (library.returnItem(bookNameToReturn, Movie.class, loggedUser)) {
            console.display(THANK_YOU_FOR_RETURNING_THE_MOVIE);
        } else {
            console.display(THAT_IS_NOT_A_VALID_MOVIE_TO_RETURN);
        }
    }

}
