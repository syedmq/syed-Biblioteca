package com.pathashala57.controller;

import com.pathashala57.model.Library;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

import java.util.Map;

//Represents menu before user login
class Menu {

    private static final String WELCOME_TO_BANGALORE_PUBLIC_LIBRARY =
            "Welcome to Bangalore Public Library!";
    private static final String MENU = "Menu:";
    private static final String PLEASE_ENTER_YOUR_CHOICE = "Please Enter Your Choice:";
    private final Console console;
    private final Library library;
    private final Map<String, MenuOption> menuMap;
    private final User user;

    Menu(Console console, Library library, Map<String, MenuOption> menuMap, User user) {
        this.console = console;
        this.library = library;
        this.menuMap = menuMap;
        this.user = user;
    }

    void displayWelcomeMessage() {
        console.display(WELCOME_TO_BANGALORE_PUBLIC_LIBRARY);
    }

    void showChoices() {
        boolean runAgain = true;
        do {
            console.display(MENU);
            displayOptions();
            console.display(PLEASE_ENTER_YOUR_CHOICE);
            String choice = console.getInput();
            MenuOption selectedOption = menuMap.getOrDefault(choice, new InvalidInput());
            selectedOption.execute(library, console, user);
            if (selectedOption.getClass() == QuitMenu.class) {
                runAgain = false;
            }
        } while (runAgain);
    }

    private void displayOptions() {
        for (String key : menuMap.keySet()) {
            console.display("Enter:  " + key + "   To   " + (menuMap.get(key)).getName());
        }
    }

    void run() {
        displayWelcomeMessage();
        showChoices();
    }

}
