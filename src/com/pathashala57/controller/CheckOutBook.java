package com.pathashala57.controller;

import com.pathashala57.model.Book;
import com.pathashala57.model.Library;
import com.pathashala57.model.LibraryItem;
import com.pathashala57.model.User;
import com.pathashala57.view.Console;

import java.util.Optional;

//Represents menu item to loan a book
public class CheckOutBook implements MenuOption {

    private static final String ENTER_THE_BOOK_NAME_YOU_WANT_TO_CHECKOUT =
            "Enter the Book Name You Want To Checkout";
    private static final String THANK_YOU_ENJOY_THE_BOOK = "Thank you! Enjoy the book!";
    private static final String THAT_BOOK_IS_NOT_AVAILABLE = "That book is not available.";
    private static final String name = "Check Out A  Book";

    public String getName() {
        return name;
    }

    @Override
    public void execute(Library library, Console console, User loggedUser) {
        console.display(ENTER_THE_BOOK_NAME_YOU_WANT_TO_CHECKOUT);
        String bookNameToCheckout = console.getInput();
        Optional<LibraryItem> bookTocheckOut = library.checkout(bookNameToCheckout, Book.class, loggedUser);
        if (bookTocheckOut.isPresent()) {
            console.display(THANK_YOU_ENJOY_THE_BOOK);
        } else {
            console.display(THAT_BOOK_IS_NOT_AVAILABLE);
        }
    }

}
