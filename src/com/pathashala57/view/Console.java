package com.pathashala57.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

//Represents input output device
public class Console implements InputOutput {

    private PrintStream outputStream;
    private BufferedReader inputReader;

    Console(PrintStream stream) {
        this(stream, null);
    }

    Console(BufferedReader inputReader) {
        this(System.out, inputReader);
    }

    public Console(PrintStream out, BufferedReader bufferedReader) {
        this.outputStream = out;
        this.inputReader = bufferedReader;
    }

    @Override
    public void display(String message) {
        outputStream.println(message);
    }

    @Override
    public String getInput() {
        String input = "";
        try {
            input = inputReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

}
