package com.pathashala57.view;


interface InputOutput {

    void display(String message);

    String getInput();
}
