package com.pathashala57.model;


import java.util.*;
import java.util.stream.Collectors;

//Represents the virtual library
public class Biblioteca implements Library {

    private final List<LibraryItem> libraryItems;
    private final List<User> users;
    private final List<LibraryItem> libraryItemsCheckedOut;
    private Map<LibraryItem, User> libraryRegister;

    public Biblioteca(List<LibraryItem> LibraryItems, List<User> users) {
        this.libraryItems = new ArrayList<>(LibraryItems);
        this.users = users;
        libraryItemsCheckedOut = new ArrayList<>();
        libraryRegister = new HashMap<>();
    }

    public Optional<LibraryItem> checkout(String itemNameToCheckout, Class<? extends LibraryItem> itemClass, User aUser) {
        Optional<LibraryItem> itemToCheckOut = libraryItems.stream()
                .filter(LibraryItem -> LibraryItem.hasName(itemNameToCheckout))
                .filter(LibraryItem -> LibraryItem.getClass() == itemClass)
                .findFirst();

        if (itemToCheckOut.isPresent()) {
            LibraryItem libraryItem = itemToCheckOut.get();
            libraryItems.remove(libraryItem);
            libraryItemsCheckedOut.add(libraryItem);
            libraryRegister.put(libraryItem, aUser);

        }
        return itemToCheckOut;
    }

    public boolean returnItem(String itemNameToReturn, Class<? extends LibraryItem> itemClass, User aUser) {
        Optional<LibraryItem> itemToReturn = libraryItemsCheckedOut.stream()
                .filter(LibraryItem -> LibraryItem.hasName(itemNameToReturn))
                .filter(LibraryItem -> LibraryItem.getClass() == itemClass)
                .findFirst();
        if (itemToReturn.isPresent()) {
            LibraryItem libraryItem = itemToReturn.get();
            if (!(libraryRegister.get(libraryItem).equals(aUser))) {
                return false;
            }

            libraryItems.add(libraryItem);
            libraryItemsCheckedOut.remove(libraryItem);
            return true;
        }
        return false;
    }

    @Override
    public Optional<User> verifyUser(String userid, String password) {
        return users.stream()
                .filter(User -> User.verifyCredentials(userid, password))
                .findFirst();
    }

    public String listOfItems(Class<? extends LibraryItem> itemClass) {
        List<LibraryItem> itemsList = libraryItems
                .stream()
                .filter(LibraryItem -> LibraryItem.getClass() == itemClass)
                .collect(Collectors.toList());

        StringBuffer bookDetails = new StringBuffer();
        for (LibraryItem aBook : itemsList) {
            bookDetails.append(aBook.getStringRepresentation() + System.lineSeparator());
        }
        return (bookDetails.toString());
    }

    public boolean hasNoItemsOf(Class<? extends LibraryItem> itemClass) {
        return libraryItems.stream().noneMatch(item -> item.getClass() == itemClass);
    }

}
