package com.pathashala57.model;

//Represent a bound set of pages
public class Book implements LibraryItem {

    private final String name;
    private final String author;
    private final int date;

    public Book(String name, String author, int date) {
        this.name = name;
        this.author = author;
        this.date = date;
    }

    public String getStringRepresentation() {
        String headerformat = "%-35s %-35s %-35s";
        return String.format(headerformat, name, author, date);
    }

    public boolean hasName(String bookNameToCheckout) {
        return ((name.toLowerCase()).equals(bookNameToCheckout.toLowerCase()));
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (that == null)
            return false;
        if (that.getClass() != this.getClass())
            return false;
        Book anotherBook = (Book) that;

        return anotherBook.name.equals(this.name) &&
                anotherBook.author.equals(this.author) &&
                anotherBook.date == this.date;
    }

}
