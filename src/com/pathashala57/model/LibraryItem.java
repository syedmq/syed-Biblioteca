package com.pathashala57.model;

//Represents items that the library can have
public interface LibraryItem {

    String getStringRepresentation();

    boolean hasName(String name);
}
