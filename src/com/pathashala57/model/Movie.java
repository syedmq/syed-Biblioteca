package com.pathashala57.model;

//Represents continous pictures with sound

public class Movie implements LibraryItem {

    private final String name;
    private final String director;
    private final int date;

    public Movie(String name, String director, int date) {

        this.name = name;
        this.director = director;
        this.date = date;
    }

    public String getStringRepresentation() {
        String headerformat = "%-35s %-35s %-35s";
        return String.format(headerformat, name, director, date);
    }

    @Override
    public boolean hasName(String givenName) {
        return ((this.name.toLowerCase()).equals(givenName.toLowerCase()));
    }

}
