package com.pathashala57.model;

import java.util.Optional;

//Represents a template for Bibloteca
public interface Library {

    Optional<LibraryItem> checkout(String itemNameToCheckout, Class<? extends LibraryItem> itemClass, User aUser);

    boolean hasNoItemsOf(Class<? extends LibraryItem> itemClass);

    String listOfItems(Class<? extends LibraryItem> itemClass);

    boolean returnItem(String itemNameToReturn, Class<? extends LibraryItem> itemClass, User aUser);

    Optional<User> verifyUser(String userid, String password);
}
