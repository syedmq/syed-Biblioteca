package com.pathashala57.model;

//Represents a human who is a member of the library
public class User {

    private final String name;
    private final String id;
    private final String password;
    private final String phone;
    private final String email;

    public User(String name, String id, String password, String phone, String email) {
        this.name = name;
        this.id = id;
        this.password = password;
        this.phone = phone;
        this.email = email;
    }

    public boolean verifyCredentials(String userid, String password) {
        return this.id.equals(userid) && this.password.equals(password);
    }

    public String stringRepresentation() {
        String headerformat = "%-35s %-10s %-20s %-20s";
        return String.format(headerformat, name, id, phone, email);

    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (!(that.getClass() == User.class)) {
            return false;
        }
        User anotherUser = (User) that;
        return id.equals(anotherUser.id);
    }
}
